package com.heyongjie.pdf_opea.util;

import cn.hutool.core.util.StrUtil;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.SimpleBookmark;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.bookmarks.PdfBookmark;
import com.spire.pdf.bookmarks.PdfBookmarkCollection;
import com.spire.pdf.bookmarks.PdfTextStyle;
import com.spire.pdf.graphics.PdfRGBColor;
import org.springframework.util.StringUtils;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class PdfRead {

    // 存储目录信息
    public static List<CatalogueInfo> ALL_CATALOGUE = new ArrayList<>();

    // 存储页数和标题的对应关系
    public static Map<String,String > titleInfo = new Hashtable<>();

    /**
     * 读取目录
     * dir.txt 内容我是从jd找到得， 一般每本书得详情里面都有目录
     * @param lineNum   目录对应得文件有多少行
     * @throws Exception  Exception
     */
    public static void readCatalogue(int lineNum) throws Exception{
        List<String> txtInfo = new ArrayList<>();
        // 存储目录的文件
        File file = new File("C:\\Users\\Admin\\Desktop\\demo\\src\\main\\resources\\dir.txt");

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line = "";

        while(lineNum > 0){
            line =  bufferedReader.readLine();
            if(!StrUtil.isBlank(line)){
                txtInfo.add(line);
            }
            lineNum--;
        }

        ALL_CATALOGUE = txtInfo.stream().map(item -> {
            String[] s =item.split(" ");
            CatalogueInfo catalogueInfo = new CatalogueInfo();
            catalogueInfo.setName(s[0]);
            catalogueInfo.setPageNum(Integer.parseInt(s[1]));
            // 避免后面的覆盖前面的
            if(!titleInfo.containsKey(s[1])){
                titleInfo.put(s[1], s[0]);
            }

            return catalogueInfo;
        }).collect(Collectors.toList());
        bufferedReader.close();
    }


    public static void main(String[] args) throws Exception{
        // 获取目录信息

        readCatalogue(237);
        PdfReader reader = new PdfReader("C:\\Users\\Admin\\Desktop\\Java核心技术 卷2 高级特性（原书第11版）.com.pdf");
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream("C:\\Users\\Admin\\Desktop\\Java核心技术 卷2 高级特性（原书第11版）带目录.pdf"));
        List<HashMap<String, Object>> list = SimpleBookmark.getBookmark(reader);
        changeList(list);

        stamper.setOutlines(list);
        stamper.close();
        reader.close();



    }

    public static void changeList(List<HashMap<String, Object>> list) {
        for (HashMap<String, Object> entry : list) {
            for (String key : entry.keySet()) {
                if("Title".equals(key)){
                    String value = entry.get(key) == null ? "" : entry.get(key).toString();
                    if(StrUtil.isNotBlank(value)){
                        String s = titleInfo.get(value);
                        if(StrUtil.isNotBlank(s)){
                            entry.put("Title",s);
                        }else {
                            // 如果是纯数字， 取消该书签
                            try{
                                Integer.parseInt(value);
                                entry.put("Title",null);
                            }catch (Exception e){ }
                        }
                    }

                }
            }
        }


        // 如果书签为空移除
        list.removeIf(next -> StrUtil.isBlank(next.get("Title") == null ? "" : next.get("Title").toString()));
    }

}
